import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { CustomValidators } from '../../validators/custom.validators'; 
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  fg : FormGroup

  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.fg = new FormGroup({
      'Email' : new FormControl('', Validators.required, CustomValidators.CheckEmailAsyncValidator(this.httpClient))
    });
  }

}
