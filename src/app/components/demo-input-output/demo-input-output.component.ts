import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo-input-output',
  templateUrl: './demo-input-output.component.html',
  styleUrls: ['./demo-input-output.component.scss']
})
export class DemoInputOutputComponent implements OnInit {

  color: string

  resultatFromChildren: boolean

  constructor() { }

  ngOnInit(): void {
  }

  changeColor() {
    if(this.color == 'success')
      this.color = 'danger';
    else{
      this.color = 'success';
    }
  }

  test(b: boolean) {
    this.resultatFromChildren = b;
  }

}
