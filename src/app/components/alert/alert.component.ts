import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {

  @Input()
  set color(value) {
    console.log(value);
    this._color = value
  }

  _color: string;

  @Output()
  toto: EventEmitter<boolean>


  constructor() {
    this.toto = new EventEmitter<boolean>();
  }

  ngOnInit(): void {
  }

  click(b: boolean) {
    this.toto.emit(b)
  }

}
