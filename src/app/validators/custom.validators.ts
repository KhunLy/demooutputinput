import { AsyncValidatorFn } from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import { map, delay } from 'rxjs/operators';
export class CustomValidators {


    // static readonly URL:string =
    //     'http://loicbaudoux.somee.com/api/user/verifyEmail/';
    static readonly URL:string =
        'https://restcountries.eu/rest/v2/all';

    public static CheckEmailAsyncValidator(client: HttpClient) : AsyncValidatorFn  {
        return (control) => {
            // return client.post(this.URL, { email : control.value })
            //     .pipe(map(r => {
            //         if(r != null) return { emailExists: 'Cet Email existe déjà' }
            //         else return null
            //     }));
            return client.get<any>(this.URL)
                .pipe(map(r => {
                    if(r.find(p => p.name == control.value))
                        return { emailExists: 'Cet Email existe déjà' }
                    else
                        return null
                }), delay(5000));
        }
    }
}